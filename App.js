import React, { Component } from 'react';
import { Provider} from 'react-redux';
import createStore from './app/store/createStore';
import Main from "./app/main.js";

export default class App extends Component {
  render() {
    const store = createStore();
    return (
      <Provider store={store}>
      <Main />
      </Provider>
    );
  }
}