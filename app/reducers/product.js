import * as types from '../const/types';

export  function allProduct(state = null, action) {
	switch (action.type) {
		case types.ALL_PRODUCT:
			return action.product;
		default:
			return state;
	}
};

export  function currentProduct(state = null, action) {
	switch(action.type) {
		case types.CURRENT_PRODUCT:
			return action.product;
		default:
			return state;
	}
}

export  function currentAuctions(state = null, action) {
	switch(action.type) {
		case types.CURRENT_WON_AUCTIONS:
			return action.product;
		default:
			return state;
	}
}