import * as types from '../const/types';

export default function currentUser(state = null, action) {
	switch (action.type) {
		case types.CURRENT_USER:
			return action.user;
		default:
			return state;
	}
};