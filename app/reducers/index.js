import { combineReducers } from 'redux';
import * as types from '../const/types';
import currentUser from './user';
import {allProduct, currentProduct, currentAuctions} from './product';


export const updateNavId = val => ({
    type: types.NAV_INDEX,
    val,
});
export const updateAuthVal = val => ({
	type: types.UPDATE_ISAUTH,
	val,
});
export const updateProductNav = val => ({
	type:types.UPDATE_PRODUCT_NAV,
	val
})

const currentNavId = (state = 1, action) => {
    switch (action.type) {
		case types.NAV_INDEX:
			return action.val;
		default:
			return state;
	}
};
const currentAuthVal = (state = false, action) => {
	switch (action.type) {
		case types.UPDATE_ISAUTH:
			return action.val;
		default:
			return state;
	}
};
const currentProductNav = (state = false, action) => {
	switch(action.type){
		case types.UPDATE_PRODUCT_NAV:
			return action.val;
		default:
			return state;
	}
}
export default combineReducers({
	currentUser,
	currentAuthVal,
	currentNavId,
	allProduct,
	currentProduct,
	currentProductNav,
	currentAuctions,
});