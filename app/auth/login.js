//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
	TouchableOpacity,
	ActivityIndicator,
	TextInput,
	
} from 'react-native';
import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
import { bindActionCreators } from 'redux';
import { Login } from '../actions/user';
import images from '../const/images';

// create a component
class LoginScreen extends Component {
    constructor() {
        super();

        this.state = {
			login_email: '',
			login_password: '',
			loading:false,
		};
    }

    login() {
		
		const { login_email, login_password} = this.state;
		const { Login, updateAuthVal } = this.props;
		this.setState({ loading: true});
		Login(login_email, login_password)
		.then(() => {
			this.setState({ loading: false });
			this.props.navigation.navigate('HomeNav');
			// updateAuthVal(true);
			// setTimeout(() => {
				
			// }, 2000);
		})
		.catch(() => {
			this.setState({ loading: false });
		})
    }

    render() {
		const {login_email, login_password} = this.state;
        return (
            <View style={styles.container}>
                <ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
                    <View style={styles.headerDiv}>
                        <TouchableOpacity style={{ width: 40, alignItems:'center', justifyContent:'center', marginLeft: 10, marginTop: 16, }} onPress={() => this.props.navigation.goBack()}>
                            <Image source={images.backIcon} style={styles.backIcon} resizeMode="stretch" />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.bodyDiv}>
                        <View style = {{ height:250}}>
                            <Text style={styles.headerTxt}>Welcome back</Text>
                            <View style = {{marginTop:50}}>
                                <Text style = {{fontSize:11, color:'#b88581'}}>EMAIL</Text>
                                <TextInput style = {styles.input} value = {login_email} placeholder = "Email" underlineColorAndroid = 'transparent' autoCapitalize = 'none' onChangeText={text => this.setState({ login_email: text })}/>
                            </View>
                            <View style = {{marginTop:20}}>
                                <Text style={{ fontSize: 11, color:'#b88581' }}>PASSWORD</Text>
								<TextInput style={styles.input} value={login_password} placeholder="Any your password ?  " underlineColorAndroid='transparent' autoCapitalize='none' onChangeText={text => this.setState({ login_password: text })} secureTextEntry/>
                            </View>
                        </View>
                        
						<View style={{ alignItems: 'center', backgroundColor: 'transparent' }}>
                            <TouchableOpacity style={styles.boxShadow} onPress = {this.login.bind(this)}>
                                <Image source={images.login_btn} style={styles.button} resizeMode="stretch" />
                            </TouchableOpacity>
                            
                            <TouchableOpacity style={styles.forgotbtn}>
                                <Text style={styles.font}>Forgot Password?</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
				{
				this.state.loading === true ?
                        <View style = {{position:'absolute', top:0, width:width(100), height:height(100), backgroundColor:'#fff9', alignItems:'center', justifyContent:'center'}}>
                            <ActivityIndicator size="large" color="#b88581" />
                        </View>
                    : null
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	headerDiv: {
		height: height(10),
		width: width(100),
		justifyContent: 'center',
	},
	bodyDiv: {
		height: Platform.select({ ios: height(85), android: height(80) }),
		width: width(85),
		justifyContent: 'space-between',
	},
	headerTxt: {
		color: 'black',
		fontSize: 30,
		fontWeight: 'bold',
		width: Platform.select({ ios: width(70), android: width(65) }),
	},
	background: {
		width: width(100),
		height: height(100),
		alignItems: 'center',
	},
	logo: {
		width: '100%',
		height: 88,
	},
	boxShadow: {
		borderRadius: 8,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor: 'transparent',
	},
	button: {
		width: width(85),
		height: 50,
	},
	forgotbtn: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 180,
		height: 35,
		marginTop: 18,
	},
	font: {
		fontSize: 16,
		color: '#999',
	},
	backIcon: {
		width: 20,
		height: 16,

	},
	inputDiv: {
		marginTop: 40,
	},
	input: {
		height: 40,
		borderBottomWidth: 0.5,
		borderBottomColor: '#AAA',
	},
});

const mapStateToProps = store => ({
	currentAuthVal: store.currentAuthVal

});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
	Login,
	updateAuthVal: ducks.updateAuthVal
  }, dispatch);
}
//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginScreen);
