//import liraries
import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Platform,
	ScrollView,
	ImageBackground,
	Image,
	TouchableOpacity,
	TextInput,
} from 'react-native';
import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
// import { bindActionCreators } from 'redux';
import images from '../const/images';

// create a component
class GateScreen extends Component {
	constructor() {
		super();

		this.state = {};
	}
	render() {
		return (
			<View style={styles.container}>
				<ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
					<View style={styles.headerDiv} />
					<View style={styles.bodyDiv}>
						<Image source={images.logo} style={styles.logo} resizeMode="contain" />
						<View style={{ alignItems: 'center', backgroundColor: 'transparent'}}>
							<TouchableOpacity style={styles.boxShadow} onPress = {() => this.props.navigation.navigate('Signup1')}>
								<Image source={images.signup_btn} style={styles.button} resizeMode="stretch" />
							</TouchableOpacity>
                            <TouchableOpacity style={styles.loginbtn} onPress={() => this.props.navigation.navigate('FaceLogin')}>
								<Text style={styles.font}>Login</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ImageBackground>
			</View>
		);
	}
}

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	headerDiv: {
		height: height(25),
        width: width(100),
	},
	bodyDiv: {
		height: Platform.select({ ios: height(70), android: height(65) }),
		width: width(85),
        justifyContent: 'space-between',
	},
	background: {
        width: width(100),
        height: height(100),
		alignItems: 'center',
	},
	logo: {
		width: '100%',
		height: 150,
	},
	boxShadow: {
		borderRadius: 8,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor: 'transparent',
	},
	button: {
		width: width(85),
		height: 50,
	},
	loginbtn: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 50,
		height: 35,
		marginTop: 18,
	},
	font: {
		fontSize: 16,
		color: '#999',
	},
});

const mapStateToProps = store => ({

});

const mapDispatchToProps = {

};

//make this component available to the app
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(GateScreen);
