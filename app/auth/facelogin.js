//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
// import { bindActionCreators } from 'redux';
import images from '../const/images';

// create a component
class FaceLoginScreen extends Component {
    constructor() {
        super();

        this.state = {};
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
                    <View style={styles.headerDiv}>
                        <TouchableOpacity style={{ width: 40, alignItems:'center', justifyContent:'center', marginLeft: 10, marginTop: 16, }} onPress={() => this.props.navigation.goBack()}>
                            <Image source = {images.backIcon} style = {styles.backIcon} resizeMode = "stretch" />
                        </TouchableOpacity>
                    </View>
                    
                    <View style={styles.bodyDiv}>
                        <Text style = {styles.headerTxt}>Would you like to log in with Face ID?</Text>
                        <Image source={images.faceID} style={styles.logo} resizeMode="contain" />
                        <View style={{ alignItems: 'center', backgroundColor:'transparent' }}>
                            <TouchableOpacity style={styles.boxShadow}>
                                <Image source={images.face_btn} style={styles.button} resizeMode="stretch" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.nothanksbtn} onPress={() => this.props.navigation.navigate('Login')}>
                                <Text style={styles.font}>No Thanks</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	headerDiv: {
		height: height(10),
		width: width(100),
		justifyContent: 'center',
	},
	bodyDiv: {
		height: Platform.select({ ios: height(85), android: height(80) }),
		width: width(85),
		justifyContent: 'space-between',
	},
	headerTxt: {
		color: 'black',
		fontSize: 30,
		fontWeight: 'bold',
		width: Platform.select({ ios: width(70), android: width(65) }),
	},
	background: {
		width: width(100),
		height: height(100),
		alignItems: 'center',
	},
	logo: {
		width: '100%',
		height: 88,
	},
	boxShadow: {
		borderRadius: 8,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor: 'transparent',
	},
	button: {
		width: width(85),
		height: 50,
	},
	nothanksbtn: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 100,
		height: 35,
		marginTop: 18,
	},
	font: {
		fontSize: 16,
		color: '#999',
	},
	backIcon: {
		width: 20,
		height: 16,
	},
});

const mapStateToProps = store => ({

});

const mapDispatchToProps = {

};

//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FaceLoginScreen);
