//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    KeyboardAvoidingView
} from 'react-native';
import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
// import { bindActionCreators } from 'redux';
import images from '../const/images';
import { bindActionCreators } from 'redux';
import { Signup } from '../actions/user';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
// create a component
class Signup2Screen extends Component {
    constructor() {
        super();
        
        this.state = {
            photo:null,
            name : '',
            surname: '',
            address: '',
            phone: '',
            loading:false,
        };
        
    }
    signup(){
        const { photo, name, surname, address, phone } = this.state;
        const { username, email, password} = this.props.navigation.state.params;
        const { Signup, updateAuthVal} = this.props;
        this.setState({loading:true});
        Signup(email, password, username, photo, name, surname, address, phone)
        .then(()=>{
            this.setState({ loading: false });
            this.props.navigation.navigate('HomeNav');
            // updateAuthVal(true);
            // setTimeout(() => {
                
            // }, 2000);
            
        })
        .catch(() =>{
            this.setState({ loading: false });
        })
    }
    imagepicker(){
        ImagePicker.showImagePicker({}, (response) =>{
            if(response.didCancel){
                console.log('User cancelled image picker');
            }
            else if(response.error){
                console.log('ImagePicker Error', response.error);
            }
            else {
                ImageResizer.createResizedImage(response.uri, 200, 200, 'PNG', 100).then((res) =>{
                    let source = {uri: res.uri, type : 'image/jpg', name: res.name};
                    this.setState({
                        photo: source
                    })
                }).catch((err) =>{
                    alert(err)
                })
            }
        });
        
    }
    render() {

        const { photo, name, surname, address, phone } = this.state;
        return <View style={styles.container}>
				<ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
					<View style={styles.headerDiv}>
						<TouchableOpacity style={{ width: 40, alignItems:'center', justifyContent:'center', marginLeft: 10, marginTop: 16, }} onPress={() => this.props.navigation.goBack()}>
							<Image source={images.backIcon} style={styles.backIcon} resizeMode="stretch" />
						</TouchableOpacity>
					</View>

					<View style={styles.bodyDiv}>
                        <KeyboardAwareScrollView extraHeight={Platform.select({android : 120, ios:0}) } automaticallyAdjustContentInsets={false} enableOnAndroid={true} keyboardShouldPersistTaps='handled'>
                            <View style={{ height: 450, justifyContent: 'space-between' }}>
                                <Text style={styles.headerTxt}>Add personal data</Text>
                                <View style = {{flexDirection:'row', justifyContent:'space-between',  alignItems:'center'}}>
                                    <TouchableOpacity style = {styles.photoDiv} onPress = {this.imagepicker.bind(this)}>
                                        <Image source = {this.state.photo === null ? images.userIcon : this.state.photo} style = {this.state.photo === null ? styles.smallPhotoSize : styles.photoSize} resizeMode = "stretch" />
                                    </TouchableOpacity>
                                    <Text style = {{fontSize:16, color:'black', width:width(50)}}>Upload Profile Picture</Text>
                                </View>
                                <View style = {{height:200,justifyContent:'space-between'}}> 
                                    <View style = {{flexDirection : 'row'}}>
                                        <View style={{  width:width(40), marginRight:width(5) }}>
                                            <Text style={{ fontSize: 11, color: '#b88581' }}>NAME</Text>
                                            <TextInput style={styles.input} placeholder="Name" value = {name} underlineColorAndroid="transparent" autoCapitalize="none" onChangeText={text => this.setState({ name: text })}/>
                                        </View>
                                        <View style={{width:width(40) }}>
                                            <Text style={{ fontSize: 11, color: '#b88581' }}>SURENAME</Text>
                                            <TextInput style={styles.input} placeholder="Surename" value = {surname} underlineColorAndroid="transparent" autoCapitalize="none" onChangeText={text => this.setState({ surname: text })}/>
                                        </View>
                                    </View>
                                    <View style={{ }}>
                                        <Text style={{ fontSize: 11, color: '#b88581' }}>ADDRESS</Text>
                                        <View style = {{flexDirection:'row', alignItems:'center', borderBottomWidth:0.5, borderBottomColor : '#AAA'}}>
                                            <TextInput style={[styles.input, {width:width(85)-20, borderBottomWidth:0}]} placeholder="Address" value = {address} underlineColorAndroid="transparent" autoCapitalize="none" onChangeText={text => this.setState({ address: text })}/>
                                            <Image source = {images.mapIcon} style = {{width:20, height:20}} resizeMode = "stretch" />
                                        </View>
                                    </View>
                                    <View style={{ }}>
                                        <Text style={{ fontSize: 11, color: '#b88581' }}>PHONE NUMBER</Text>
                                        <TextInput style={styles.input} placeholder="Phone Number" underlineColorAndroid="transparent" value = {phone} autoCapitalize="none" keyboardType= "numeric" onChangeText={text => this.setState({ phone: text })}/>
                                    </View>
                                </View>
                            </View>
                        </KeyboardAwareScrollView>
						
					</View>
                    <View style = {{flex:1, backgroundColor:'transparent'}}>
                        {/* <View style={{ alignItems: 'center'}}> */}
                                <TouchableOpacity style={styles.boxShadow} onPress = {this.signup.bind(this)}>
                                    <Image source={images.signup_btn} style={styles.button} resizeMode="stretch" />
                                </TouchableOpacity>
                        {/* </View> */}
                    </View>
				</ImageBackground>
                {
                    this.state.loading === true ?
                        <View style = {{position:'absolute', top:0, width:width(100), height:height(100), backgroundColor:'#fff9', alignItems:'center', justifyContent:'center'}}>
                            <ActivityIndicator size="large" color="#b88581" />
                        </View>
                    : null
                }
			</View>;
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerDiv: {
        height: height(10),
        width: width(100),
        justifyContent: 'center',

    },
    bodyDiv: {
        height: Platform.select({ ios: height(75), android: height(75) }),
        width: width(85),
        justifyContent: 'space-between',
        
    },
    headerTxt: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        width: Platform.select({ ios: width(70), android: width(65) }),

    },
    background: {
        width: width(100),
        height: height(100),
        alignItems: 'center',
    },
    logo: {
        width: '100%',
        height: 88,
    },
    boxShadow: {
        borderRadius: 8,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#AAA',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor: 'transparent',
    },
    button: {
        width: width(85),
        height: 50,
    },
    font: {
        fontSize: 16,
        color: '#999',
    },
    backIcon: {
        width: 20,
        height: 16,

    },
    inputDiv: {
        marginTop: 40,
    },
    input: {
        height: 40,
        borderBottomWidth: 0.5,
        borderBottomColor: '#AAA'
    },
    photoDiv: {
        width:100,
        height:100,
        borderWidth:2,
        borderRadius:10,
        borderColor:'#cc9f9b',
        alignItems:'center',
        justifyContent:'center',
    },
    photoSize:{
        width:100,
        height:100,
        borderRadius:10,
    },

    smallPhotoSize:{
        width:30,
        height:30,
    }

});

const mapStateToProps = store => ({


});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        Signup,
        updateAuthVal: ducks.updateAuthVal
    }, dispatch);
};

//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Signup2Screen);
