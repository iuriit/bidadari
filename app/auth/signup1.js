//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
// import { bindActionCreators } from 'redux';
import images from '../const/images';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// create a component
class Signup1Screen extends Component {
    constructor() {
        super();

        this.state = {
            username: '',
            email: '',
            password: '',
        };
    }
    render() {

        const { username, email, password } = this.state;
        return <View style={styles.container}>
				<ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
					<View style={styles.headerDiv}>
						<TouchableOpacity style={{ width: 40, alignItems:'center', justifyContent:'center', marginLeft: 10, marginTop: 16, }} onPress={() => this.props.navigation.goBack()}>
							<Image source={images.backIcon} style={styles.backIcon} resizeMode="stretch" />
						</TouchableOpacity>
					</View>

					<View style={styles.bodyDiv}>
						<KeyboardAwareScrollView enableOnAndroid resetScrollToCoords={{ x: 0, y: 0 }} contentContainerStyle={{ flexGrow: 1 }} scrollEnabled>
							<View style={{ height: 350 }}>
								<Text style={styles.headerTxt}>Create Account</Text>
								<View style={{ marginTop: 50 }}>
									<Text style={{ fontSize: 11, color: '#b88581' }}>USERNAME</Text>
									<TextInput style={styles.input} placeholder="Username" value = {username} underlineColorAndroid="transparent" autoCapitalize="none" onChangeText={text => this.setState({ username: text })}/>
								</View>
								<View style={{ marginTop: 20 }}>
									<Text style={{ fontSize: 11, color: '#b88581' }}>EMAIL</Text>
                                <TextInput style={styles.input} placeholder="Email" value={email} underlineColorAndroid="transparent" autoCapitalize="none" onChangeText={text => this.setState({ email: text })}/>
								</View>
								<View style={{ marginTop: 20 }}>
									<Text style={{ fontSize: 11, color: '#b88581' }}>PASSWORD</Text>
									<TextInput style={styles.input} placeholder="Any your password ?  " value = {password} underlineColorAndroid="transparent" autoCapitalize="none" secureTextEntry onChangeText={text => this.setState({ password: text })}/>
								</View>
							</View>
						</KeyboardAwareScrollView>
					</View>
                    <View style={{ alignItems: 'center'}}>
							<TouchableOpacity style={styles.boxShadow} onPress={() => this.props.navigation.navigate('Signup2', {username, email, password})}>
								<Image source={images.continuebtn} style={styles.button} resizeMode="stretch" />
							</TouchableOpacity>
					</View>
				</ImageBackground>
			</View>;
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerDiv: {
        height: height(10),
        width: width(100),
        justifyContent: 'center',
        
    },
    bodyDiv: {
        height: Platform.select({ ios: height(75), android: height(75) }),
        width: width(85),
        justifyContent: 'space-between',
    },
    headerTxt: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        width: Platform.select({ ios: width(70), android: width(65) }),
       
    },
    background: {
        width: width(100),
        height: height(100),
        alignItems: 'center',
    },
    logo: {
        width: '100%',
        height: 88,
    },
    boxShadow: {
        borderRadius: 8,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#AAA',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor: 'transparent',
    },
    button: {
        width: width(85),
        height: 50,
    },
    font: {
        fontSize: 16,
        color: '#999',
    },
    backIcon: {
        width: 20,
        height: 16,
    },
    inputDiv:{
        marginTop:40,
    },
    input: {
        height:40,
        borderBottomWidth : 0.5,
        borderBottomColor : '#AAA'
    }
    
});

const mapStateToProps = store => ({

});

const mapDispatchToProps = {

};

//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Signup1Screen);
