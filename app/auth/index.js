import { Easing, Animated } from 'react-native';
import { StackNavigator } from 'react-navigation';


import LoginScreen from './login';
import GateScreen from './gate';
import FaceLoginScreen from './facelogin';
import Signup1Screen from './signup1';
import Signup2Screen from './signup2';


const fade = (props) => {
    const { position, scene } = props

    const index = scene.index

    const translateX = 0
    const translateY = 0

    const opacity = position.interpolate({
        inputRange: [index - 0.7, index, index + 0.7],
        outputRange: [0.3, 1, 0.3]
    })

    return {
        opacity,
        transform: [{ translateX }, { translateY }]
    }
}

export default AuthNav = StackNavigator({
    Gate: {
        screen: GateScreen
    },
    Login: {
        screen: LoginScreen
    },
    FaceLogin: {
        screen: FaceLoginScreen
    },
    Signup1: {
        screen: Signup1Screen
    },
    Signup2: {
        screen: Signup2Screen
    },

}, {
        headerMode: 'none',
        navigationOptions: {
            gesturesEnabled: false
        },
        transitionConfig: () => ({
            transitionSpec: {
                duration: 500,
                useNativeDriver: true,
                easing: Easing.out(Easing.poly(4)),
                timing: Animated.timing,
            },
            screenInterpolator: (props) => {
                return fade(props)
            }
        })
    })