import * as types from '../const/types';
import { get_endingsoon_product, get_wonauctions, get_mostview_product, get_mostbid_product, submit_bid, get_product } from '../api/product';


export const Get_Ending_Product = (token) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            get_endingsoon_product(token)
				.then(res => {
					res.json().then(json => {
						if (json.products !== undefined) {
                            const product = [];
							product = json.products;
                            dispatch({ type: types.ALL_PRODUCT, product });
							resolve();
						} else {
							alert(JSON.stringify(json.errors));
							reject();
						}
					});
				})
				.catch(err => {
					alert(JSON.stringify(err));
					reject();
				});
        })
    }
}

export const Get_Mostview_Product = (token) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            get_mostview_product(token)
				.then(res => {
					res.json().then(json => {
						if (json.products !== undefined) {
                            
							const product = [];
							product = json.products;
							dispatch({ type: types.ALL_PRODUCT, product });
							resolve();
						} else {
							alert(JSON.stringify(json.errors));
							reject();
						}
					});
				})
				.catch(err => {
					alert(JSON.stringify(err));
					reject();
				});
        })
    }
}
export const Get_Mostbid_Product = (token) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            get_mostbid_product(token)
                .then(res => {
                    res.json().then(json => {
                        if (json.products !== undefined) {

                            const product = [];
                            product = json.products;
                            dispatch({ type: types.ALL_PRODUCT, product });
                            resolve();
                        } else {
                            alert(JSON.stringify(json.errors));
                            reject();
                        }
                    });
                })
                .catch(err => {
                    alert(JSON.stringify(err));
                    reject();
                });
        })
    }
}

export const Get_Product = (token, id) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            get_product(token, id)
                .then(res => {
                    res.json().then(json => {
                        
                        if (json.product !== undefined) {
                            const product = [];
                            product = json.product;
                            dispatch({ type: types.CURRENT_PRODUCT, product });
                            resolve(product);
                        } else {
                            alert(JSON.stringify(json.errors));
                            reject();
                        }
                    });
                })
                .catch(err => {
                    alert(JSON.stringify(err));
                    reject();
                });
        })
    }
}

export const Submit_Bid = (token, id, price) => {
    
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            submit_bid(token, id, price)
				.then(res => {
					res.json().then(json => {
                        if(json.user !== undefined)
                        {
                            const { id, username, email, token, image, name, surname, address, phone, token_count, bid_count } = json.user;
                            const user = { id, username, email, token, image, name, surname, address, phone, token_count, bid_count };
                            dispatch({
                                type: types.CURRENT_USER,
                                user
                            });
                            resolve();
                        }
                        else{
                            alert(JSON.stringify(json));
							reject();
                        }
					});
				})
				.catch(err => {
					alert(JSON.stringify(err));
					reject();
				});
        })
    }
}

export const Get_Wonauctions = (token) => {

    return (dispatch) => {
        return new Promise((resolve, reject) => {
            get_wonauctions(token)
                .then(res => {
                    res.json().then(json => {
                        if (json.products !== undefined) {
                            const product = [];
							product = json.products;
							dispatch({ type: types.CURRENT_WON_AUCTIONS, product });
                            // alert(JSON.stringify(product));
                            resolve();

                        }
                        else {
                            alert(JSON.stringify(json));
                            reject();
                        }
                    });
                })
                .catch(err => {
                    alert(JSON.stringify(err));
                    reject();
                });
        })
    }
}