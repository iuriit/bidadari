import * as types from '../const/types';
import { login, signup } from '../api/auth';

export const Login = (email, password) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            login(email, password)
            .then(res => {
                res.json().then(json => {
                    if(json.user !== undefined){
                        const { id, username, email, token, image, name, surname, address, phone, token_count, bid_count} = json.user;
                        const user = { id, username, email, token, image, name, surname, address, phone, token_count, bid_count };
                        dispatch({
                            type:types.CURRENT_USER,
                            user
                        });
                        resolve();
                    }
                    else{
                        alert(JSON.stringify(json.errors));
                        reject();
                    }

                })
            })
            .catch(err => {
                alert(JSON.stringify(err));
                reject();
            })
        })
    }
}

export const Signup = (email, password, username, image, name, surname, address, phone) => {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            signup(email, password, username, image, name, surname, address, phone)
            .then(res => {
                
                res.json().then(json => {
                    
                    if(json.user !== undefined)
                    {
                        const { id, username, email, token, image, name, surname, address, phone } = json.user;
                        const user = { id, username, email, token, image, name, surname, address, phone };
                        dispatch({
                            type: types.CURRENT_USER,
                            user
                        });
                        resolve();
                    }
                    else{
                        alert(JSON.stringify(json.errors));
                        reject();
                    }

                })
            })
            .catch(err => {
                alert(JSON.stringify(err));
                reject();
            })
        })
    }
}