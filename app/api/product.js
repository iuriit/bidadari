export const get_endingsoon_product = (token) => {
	const apiUrl = 'http://13.250.103.147/api/products/ending_soon';
	return fetch(apiUrl, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + token,
		},
	});
};
export const get_mostbid_product = (token) => {
	const apiUrl = 'http://13.250.103.147/api/products/most_bid';
	return fetch(apiUrl, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + token,
		},
	});
};
export const get_mostview_product = (token) => {
	const apiUrl = 'http://13.250.103.147/api/products/most_view';
	return fetch(apiUrl, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + token,
		},
	});
};


export const get_product = (token, id) => {
	const apiUrl = 'http://13.250.103.147/api/products/' +id;
	return fetch(apiUrl, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + token,
		},
	});
};

export const get_wonauctions = (token) => {
	const apiUrl = 'http://13.250.103.147/api/products/won_auctions';
	return fetch(apiUrl, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + token,
		},
	});
};

export const submit_bid = (token, id, price) => {
	const apiUrl = 'http://13.250.103.147/api/bids/add';
	const body = {
			"product_id": id,
			"price":price
	};
	return fetch(apiUrl, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Token ' + token,
		},
		body: JSON.stringify(body)
	});
};