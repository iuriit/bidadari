export const signup = (email, password, username, image, name, surname, address, phone) => {
  let formdata = new FormData();
  const user = {
    email,
    password,
    username,
    image,
    name,
    surname,
    address,
    phone,
  };
  const keys = Object.keys(user)
  keys.map(key => formdata.append(key, user[key]))

  let apiUrl = 'http://13.250.103.147/api/register';
  return fetch(apiUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    body: formdata
  })
}
export const login = (email, password) => {
    const apiUrl = 'http://13.250.103.147/api/login';
    const body = {
        user: {
            email,
            password
        }
    };
    return fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    });
}