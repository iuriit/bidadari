import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';
import * as ducks from './reducers/index';
import { connect } from 'react-redux';
import SplashScreen from './src/Splash/SplashScreen';
import { bindActionCreators } from 'redux';
import AuthNav from './auth/index';
import Root from './src/root';
import { transitionConfig } from '../app/src/global';


const MainNav = StackNavigator(
	{
		LoginNav: {
			screen: AuthNav,
		},
		HomeNav: {
			screen: Root,
			navigationOptions: {
				tabBarVisible: false,
			},
		},
	},
	{
		headerMode: 'none',
		initialRouteName: 'LoginNav',
		navigationOptions: {
			gesturesEnabled: false,
		},
		transitionConfig,
	}
);

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showSplash: false,
		};
		// setTimeout(() => {
		//   this.setState({ showSplash: false });
		// }, 2500);
	}

	FacebookLogin() {

    
	}
	Login() {}
	render() {

		const {currentAuthVal} = this.props;
		if (this.state.showSplash) {
			return <SplashScreen />;
		}
		return <MainNav />;
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
});

const mapStateToProps = store => ({
   currentAuthVal: store.currentAuthVal

});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
	updateAuthVal : ducks.updateAuthVal
  }, dispatch);
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Main);
