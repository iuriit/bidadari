//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import { DrawerNavigator, StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
// import { bindActionCreators } from 'redux';
import { transitionConfig } from './global';
import images from '../const/images';
import HomeNav from './home/index';
import ProfileNav from './profile/index';
import BottomNav from './tabNav';

// create a component
const TabNav = TabNavigator(
	{
		HOME: {
			screen: HomeNav,
		},
		PROFILE: {
			screen: ProfileNav,
		},
	},
	{
		tabBarComponent: BottomNav,
		tabBarPosition: 'bottom',
		swipeEnabled: false,
		animationEnabled: false,
		initialRouteName: 'HOME',
	}
);


export default TabNav;
