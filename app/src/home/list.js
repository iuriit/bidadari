//import liraries
import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Platform,
	ScrollView,
	ImageBackground,
	Image,
	TouchableOpacity,
	TextInput,
	ActivityIndicator,
} from 'react-native';
import * as ducks from '../../reducers/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { width, height } from 'react-native-dimension';
import { Get_Ending_Product, Get_Mostview_Product, Get_Mostbid_Product, Get_Product } from '../../actions/product';
import BottomNav from '../tabNav';
import images from '../../const/images';


const scrollWidth = width(100) - 30;
// create a component
class Home extends Component {
    constructor() {
        super();

        this.state = {
            flag: 1,
            left: [],
            right: [],
            loading:false,
        };
    }
    componentWillMount() {
        
        const { products, user, Get_Ending_Product } = this.props;
        const { flag } = this.state;
        this.setState({loading:true});
        Get_Ending_Product(user.token)
        .then(() => {
            this.setState({ loading: false });
        })
        .catch(() => {
            this.setState({ loading: false });
        })
    }
    getDiffTime(endTime){
        var time = new Date(endTime);
        var curtime = new Date();
        var diff = time - curtime;
        var msec = diff;
        var dd = Math.floor(msec / 1000 / 60 / 60 / 24);
		msec -= dd * 1000 * 60 * 60 * 24;
        var hh = Math.floor(msec / 1000 / 60 / 60);
        msec -= hh * 1000 * 60 * 60;
        var mm = Math.floor(msec / 1000 / 60);
        msec -= mm * 1000 * 60;
        var ss = Math.floor(msec / 1000);
        msec -= ss * 1000;
        if(dd != 0){
            return dd + ' days left';
        }
        else if(dd==0 && hh!=0){
            return hh + ' hours left';
        }
        else if(dd==0 && hh==0 && mm != 0){
            return mm + ' minutes left';
        }
        else if(dd==0 && hh==0 && mm== 0 && ss!=0)
        {
            return ss + ' seconds left';
        }
        
        
    }
    componentWillReceiveProps(nextProps) {
        
        const {products} = nextProps;

        const data = [ ];

        const maxWidth = 140, maxHeight= 120;

        products.map((item, index) => {
            // alert(item.end_time);
            Image.getSize("http://13.250.103.147/" + item.image, (width, height) => {
                let _width = maxWidth, _height = height / width * 145;
                if (_height > maxHeight) {
                    _width = _width / _height * maxHeight;
                    _height = maxHeight;
                }
                var result = this.getDiffTime(item.end_time);
                data.push({id:item.id, image: item.image, width:_width, height:_height, name:item.name, price:item.start_price, cost:item.bid_cost, time:result, win:item.winner_image, content:item.description, bidCnt:64});
                if (index === products.length - 1) {
                    let left = [], right = [];
                    let leftHeight = 0, rightHeight = 0;
                    // let aa = 0;
                    data.map((_item, _idx) => {
                        const newHeight = _item.height + 40;
                        if (leftHeight <= rightHeight) {
                            left.push(_item);
                            leftHeight += newHeight;
                        } else {
                            right.push(_item);
                            rightHeight += newHeight;
                        }
                        // aa = _idx
                    })
                    // alert(aa)
                    this.setState({ left, right });
                }
            });
        });
    }
   
    renderImage(item, index) {
        const {Get_Product} = this.props;
        const {user} = this.props;
        return (
            <TouchableOpacity
                key={index}
                style={styles.product}
                onPress={() => {
                    this.setState({ loading: true });

                    Get_Product(user.token, item.id)
                    .then((res) => {
                        this.setState({ loading: false });
                        this.props.navigation.navigate('Detail', { res });
                    })
                    .catch(() => {  
                        this.setState({ loading: false });
                    })

                }}
            >
                <View style = {styles.time}>
                    <Text style = {[styles.small, {color:'#fff'}]}>{item.time}</Text>
                </View>
                {
                    item.image !== "" ?
                    <Image
                        source={{ uri: "http://13.250.103.147/" + item.image }}
                        style={[styles.picDiv, {width:item.width, height:item.height}]}
                        resizeMode='contain'
                    />
                    :null
                }
                <Text style = {styles.big}>{item.name}</Text>
                <View style = {styles.smallDiv}>
                    <Text style = {styles.small}>START PRICE</Text>
                    <Text style={[styles.small, { color: '#be908c' }]}>$ {item.price}</Text>
                </View>
                <View style = {[styles.smallDiv, {marginBottom : 10}]}>
                    <Text style = {styles.small}>CURRENT WINNER</Text>
                    {
                        item.win !== "" ?
                        <Image source = {{ uri: "http://13.250.103.147/" + item.win }} style = {styles.photo} resizeMode = {"stretch"} />
                        : null
                    }
                </View>
            </TouchableOpacity>
        )
    }
    endingSoon() {
        const { user, Get_Ending_Product, updateProductNav } = this.props;
        updateProductNav(1);
        this.setState({
            flag: 1
        });
        this.setState({loading:true});
        Get_Ending_Product(user.token)
        .then(() => {
            this.setState({ loading: false });
        })
        .catch(() => {
            this.setState({ loading: false });
        })
    }
    mostViewed() {
        const { user,Get_Mostview_Product, updateProductNav } = this.props;
        updateProductNav(2);
        this.setState({
            flag: 2
        });
        this.setState({loading:true});
        Get_Mostview_Product(user.token)
        .then(() => {
            this.setState({ loading: false });
        })
        .catch(() => {
            this.setState({ loading: false });
        })
    }
    mostBidded() {
        const { user, Get_Mostbid_Product, updateProductNav } = this.props;
        updateProductNav(3);
        this.setState({
            flag: 3
        });
        this.setState({loading:true});
        Get_Mostbid_Product(user.token)
        .then(() => {
            this.setState({ loading: false });
        })
        .catch(() => {
            this.setState({ loading: false });
        })
    }
    render() {
        const { left, right } = this.state;
        // alert(JSON.stringify(left));
        // alert(JSON.stringify(right));
        return (
            <View style={styles.container}>
                <ImageBackground source={images.coverImg} style={styles.coverImg} resizeMode='stretch'>
                    <Text style={styles.headerText}>What are you {'\n'} looking for today?</Text>
                </ImageBackground>
                <View style={styles.topTabBar}>
                        <TouchableOpacity style = {[this.state.flag === 1 ? styles.activeDiv : styles.inactiveDiv, {width:width(33)}]} onPress={this.endingSoon.bind(this)}>
                            <Text style={this.state.flag === 1 ? styles.activeFont : styles.inactiveFont}>ENDING SOON</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = {[this.state.flag === 2 ? styles.activeDiv : styles.inactiveDiv, {width:width(31)}]} onPress={this.mostViewed.bind(this)}>
                            <Text style={this.state.flag === 2 ? styles.activeFont : styles.inactiveFont}>MOST VIEWED</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style = {[this.state.flag === 3 ? styles.activeDiv : styles.inactiveDiv, {width:width(31)}]} onPress={this.mostBidded.bind(this)}>
                            <Text style={this.state.flag === 3 ? styles.activeFont : styles.inactiveFont}>MOST BIDDED</Text>
                        </TouchableOpacity>
                </View>
                <ScrollView contentContainerStyle={styles.mainDiv}>
                    <View style = {styles.listDiv}>
                        <View style={{ width: (scrollWidth - 10) / 2 }}>
                            {
                                left.map((item, idx) => {
                                    return this.renderImage(item, idx);
                                })
                            }
                        </View>
                        <View style={{ width: (scrollWidth - 10) / 2 }}>
                            {
                                right.map((item, idx) => {
                                    return this.renderImage(item, idx);
                                })
                            }
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.searchBar}>
                    <Image source={images.searchIcon} style={styles.searchIcon} resizeMode="stretch" />
                    <TextInput style={styles.input} placeholder="Search cars, eletronics, furniture..." underlineColorAndroid="transparent" autoCapitalize="none" />
                </View>
                {
                    this.state.loading === true ?
                        <View style = {{position:'absolute', top:0, width:width(100), height:height(100), backgroundColor:'#fff9', alignItems:'center', justifyContent:'center'}}>
                            <ActivityIndicator size="large" color="#b88581" />
                        </View>
                    : null
                }

            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
	container: {
        flex: 1,
        width: '100%',
        height: '100%',
		alignItems: 'center',
		backgroundColor: '#f5f5f5',
	},
	headerText: {
		marginLeft: 10,
		color: 'white',
		fontSize: 32,
		fontWeight: 'bold',
	},
	coverImg: {
		width: width(100),
		height: 180,
		justifyContent: 'center',
	},
	searchBar: {
		flexDirection: 'row',
		width: width(90),
		height: 50,
		backgroundColor: 'white',
		borderRadius: 10,
		position: 'absolute',
		marginTop: 155,
		alignItems: 'center',
		elevation: 2,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		// justifyContent:'center',
	},
	input: {
		width: width(75),
		height: 40,
		// backgroundColor:'grey',
	},
	searchIcon: {
		width: 16,
		height: 16,
		margin: 16,
	},
	mainDiv: {
		width: width(100),
        alignItems: 'center',
	},
	topTabBar: {
		width: width(95),
		height: 56,
		marginTop: 25,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	activeDiv: {
		backgroundColor: '#fBfBfB',
		height: 56,
		justifyContent: 'center',
		alignItems: 'center',
	},
	inactiveDiv: {
		backgroundColor: '#f5f5f5',
		height: 56,
		justifyContent: 'center',
		alignItems: 'center',
	},
	activeFont: {
		color: '#b88581',
		fontSize: 12,
	},
	inactiveFont: {
		color: '#e9c6c1',
		fontSize: 12,
	},
	time: {
		width: 100,
		height: 20,
		backgroundColor: '#b88581',
		borderBottomLeftRadius: 5,
		borderTopRightRadius: 5,
        alignSelf: 'flex-end',
        marginBottom:20,
        justifyContent:'center',
        alignItems:'center'
    },
    photo: {
        width:16,
        height:16,
        borderRadius:5,
    },
	product: {
		width: '100%',
		marginBottom: 12,
		backgroundColor: 'white',
		elevation: 2,
		borderRadius: 5,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
        shadowRadius: 2,
        alignItems:'center'
	},
	listDiv: {
		flexDirection: 'row',
		justifyContent: 'space-between',
        width: width(95),
        marginBottom:100,
    },
    big:{
        fontWeight:'bold',
        fontSize:14,
        textAlign:'center',
        color:'black',
        marginTop:15,
    },
    small:{
        fontSize:10,
        color:'#AAA',
        textAlign:'center',
        
    },
    smallDiv:{
        width: '90%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        height:20,
    },
    picDiv:{
        width:100,
        height:100,
        borderTopLeftRadius: 5, 
        borderTopRightRadius: 5, 
    },
    temppicDiv:{
        borderTopLeftRadius: 5, 
        borderTopRightRadius: 5, 
        width: 145, 
        height: 62
    }
});

const mapStateToProps = store => ({
    currentProductNav: store.currentProductNav,
	user: store.currentUser,
	products: store.allProduct,
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        Get_Product,
        Get_Ending_Product,
        Get_Mostview_Product,
        Get_Mostbid_Product,
        updateProductNav:ducks.updateProductNav,

    }, dispatch);
}

//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
