//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    Keyboard,
    ActivityIndicator
} from 'react-native';
import * as ducks from '../../reducers/index';
import { connect } from 'react-redux';
import { Submit_Bid, Get_Ending_Product, Get_Mostview_Product, Get_Mostbid_Product } from '../../actions/product';
import {NavigationActions} from 'react-navigation';
import { width, height } from 'react-native-dimension';
import { bindActionCreators } from 'redux';
import images from '../../const/images';
// create a component
class Details extends Component {
    static navigationOptions = {
        tabBarVisible: false
    }
    constructor() {
        super();

        this.state = {
            height: 0,
            bid: "",
            loading:false,
        };
    }
    getDiffTime(endTime) {
        var time = new Date(endTime);
        var curtime = new Date();
        var diff = time - curtime;
        var msec = diff;
        var dd = Math.floor(msec / 1000 / 60 / 60 / 24);
        msec -= dd * 1000 * 60 * 60 * 24;
        var hh = Math.floor(msec / 1000 / 60 / 60);
        msec -= hh * 1000 * 60 * 60;
        var mm = Math.floor(msec / 1000 / 60);
        msec -= mm * 1000 * 60;
        var ss = Math.floor(msec / 1000);
        msec -= ss * 1000;
        if (dd != 0) {
            return dd + ' days left';
        }
        else if (dd == 0 && hh != 0) {
            return hh + ' hours left';
        }
        else if (dd == 0 && hh == 0 && mm != 0) {
            return mm + ' minutes left';
        }
        else if (dd == 0 && hh == 0 && mm == 0 && ss != 0) {
            return ss + ' seconds left';
        }


    }
    componentWillMount() {
        // var result = ;
        this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this._keyboardWillShow.bind(this));
        this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this._keyboardWillHide.bind(this));

    }
    componentWillUnmount() {
        this.keyboardWillShowListener.remove();
        this.keyboardWillHideListener.remove();
    }
    _keyboardWillShow(e) {
         this.setState({ height: e.endCoordinates.height });
    }
    _keyboardWillHide(e) {
        this.setState({ height: 0 });
    }
    onBidBtn(){
        const {bid} = this.state;
        const { user, Submit_Bid, Get_Ending_Product, Get_Mostview_Product, Get_Mostbid_Product, currentProductNav } = this.props;
        if(bid != ""){
            this.setState({ loading: true });
            Submit_Bid(user.token, this.props.navigation.state.params.res.id, bid)
            .then(() => {
                
                if(currentProductNav === 1){
                    Get_Ending_Product(user.token)
                    .then(() => {
                        this.setState({ loading: false });
                    })
                    .catch(() => {
                        this.setState({ loading: false });
                    })

                }
                else if(currentProductNav === 2){
                    Get_Mostview_Product(user.token)
                    .then(() => {
                        this.setState({ loading: false });
                    })
                    .catch(() => {
                        this.setState({ loading: false });
                    })
                }
                else {
                    Get_Mostbid_Product(user.token)
                    .then(() => {
                        this.setState({ loading: false });
                    })
                    .catch(() => {
                        this.setState({ loading: false });
                    })
                }
                // alert(currentProductNav);
                Keyboard.dismiss();
                this.props.navigation.goBack();
            })
            .catch(() => {
                this.setState({ loading: false });
            })
           
        }
    }
    render() {
        const { bid } = this.state;
        return <View style={styles.container}>
                   
                    <ImageBackground source = {images.shadowImg} style = {styles.shadowImg} resizeMode = "stretch">
                        <View style={styles.headerDiv}>
                            <View style = {{flexDirection:'row',alignItems:'center', justifyContent:'space-between', width:width(95), marginTop: Platform.select({ios:16, android:0})}}>
                                <TouchableOpacity style={{ width: 40, alignItems:'center', justifyContent:'center', marginLeft: 5, marginTop: 16, }} onPress={() => {
										this.props.navigation.goBack();
										Keyboard.dismiss();
										}}>
                                    <Image source={images.backIcon} style={styles.backIcon} resizeMode="stretch" />
                                </TouchableOpacity>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                    <TouchableOpacity style={{ width: 30, alignItems:'center', marginLeft: 10, marginTop:16 }}>
                                        <Image source={images.shareIcon} style={styles.backIcon} resizeMode="contain" />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ width: 30, marginTop:16, alignItems:'center',  marginLeft: 10, }} >
                                        <Image source={images.loveIcon} style={styles.backIcon} resizeMode="contain" />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        {
                        this.props.navigation.state.params.res.image !== null 
                        ?<Image source = {{uri: "http://13.250.103.147/" + this.props.navigation.state.params.res.image}} style = {styles.imgDiv} resizeMode = "contain" />
                        :null
                        }
                        <View style = {styles.extraDiv} >
                            <Image source = {images.dotImg} style = {{width:60, height:30}} resizeMode = "contain" />
                            <View style = {styles.time}>
                                <Text style = {[styles.small, {color:'#fff'}]}>{this.getDiffTime(this.props.navigation.state.params.res.end_time)}</Text>
                            </View>
                        </View>
                    </ImageBackground>
                     {/* <KeyboardAwareScrollView> */}
                    <View style = {[styles.boxShadow,styles.info]}>
                        <ScrollView contentContainerStyle={styles.infoInner}>
                            <View style = {styles.titleDiv}>
                                <Text style = {styles.big}>{this.props.navigation.state.params.res.name}</Text>                  
                            </View>
                            <View style = {styles.statusDiv}>
                                <View style = {{width:width(28), height:50, marginRight:width(2), justifyContent:'space-between', alignItems:'center'}}>
                                    <Text style = {styles.small}>START PRICE</Text>
                                    <Text style = {[styles.big, {color:'#b88581'}]}>$ {this.props.navigation.state.params.res.start_price}</Text>
                                </View>
                                <View style = {{width:width(28), height:50, marginRight:width(2), justifyContent:'space-between', alignItems:'center'}}>
                                    <Text style = {styles.small}>BID COST</Text>
                                    <Text style = {[styles.big, {color:'#b88581'}]}>{this.props.navigation.state.params.res.bid_cost} <Text style = {[styles.small, {color:'#b88581'}]}>tokens</Text></Text>
                                </View>
                                <View style = {{width:width(28), height:50, marginRight:width(2), justifyContent:'space-between', alignItems:'center'}}>
                                    <Text style = {styles.small}>CURRENT BID</Text>
                                    <Text style = {[styles.big, {color:'#b88581'}]}>{this.props.navigation.state.params.res.bid_count}</Text>
                                </View>
                            </View>
                            <View style = {[styles.statusDiv, {alignItems:'center', justifyContent:'space-between'}]}>
                                <Text style = {styles.small}>CURRENT WINNER</Text>
                                <View style = {{flexDirection:'row', height:50, width:120, alignItems:'center', backgroundColor:'#f8f8f8', justifyContent:'space-between'}}>
                                    <Text style = {{fontSize:14, color:'black'}}>{this.props.navigation.state.params.res.winner_name}</Text>
                                    {
                                        this.props.navigation.state.params.res.winner_image !== null ?
                                        <Image source = {{ uri: "http://13.250.103.147/" + this.props.navigation.state.params.res.winner_image }} style = {{width:30, height:30, borderRadius:6}} resizeMode = "stretch" />
                                        :null
                                    }
                                </View>
                            </View>
                            <View style = {{ width:width(90), backgroundColor:'#f8f8f8', marginTop:20}}>
                                <Text style = {[styles.small, {fontSize:14}]}>{this.props.navigation.state.params.res.description}</Text>
                            </View>
                        </ScrollView>
                    </View>
                    
                    <View style = {[styles.boxShadowNav, styles.bottomDiv, {marginBottom: Platform.select ( { ios:this.state.height })}]}>
                        <View style = {styles.bottomInner}>
                            <TextInput style = {styles.inputField} value = {bid} placeholder="Enter unique price" underlineColorAndroid="transparent" autoCapitalize="none"  onChangeText={text => this.setState({ bid: text })}/>
                            <TouchableOpacity style = {styles.boxShadowBtn} onPress = {this.onBidBtn.bind(this)}>
                                <Image source = {images.bidButton} style = {styles.bidButton} resizeMode = "stretch"/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {
                        this.state.loading === true ?
                        <View style = {{position:'absolute', top:0, width:width(100), height:height(100), backgroundColor:'#fff9', alignItems:'center', justifyContent:'center'}}>
                            <ActivityIndicator size="large" color="#b88581" />
                        </View>
                    : null
                    }
                    {/* </KeyboardAwareScrollView> */}
			</View>;
    }
}

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	headerDiv: {
		height: height(10),
		width: width(100),
		flexDirection: 'row',
		alignItems: 'center',
        justifyContent: 'space-between',
        // backgroundColor:'yellow'
	},
	imgDiv: {
		width: width(90),
		height: 150,
	},
	extraDiv: {
		width: width(60),
		height: 100 - height(10),
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		alignSelf: 'flex-end',
		marginRight: width(2.5),
	},
	info: {
        backgroundColor: '#f8f8f8',
		width: width(100),
        flex: 1,
        justifyContent:'center',
        alignItems:'center',        
    },
    infoInner: {
        width:width(90),
        height:"90%",
        alignItems:'center'
    },
    titleDiv:{
        width:width(90),
        height:60,
        justifyContent:'center',
    },
    statusDiv: {
        flexDirection:'row',
        width: width(90),
        height: 60,
        justifyContent: 'center',
        borderBottomWidth:0.5,
        borderBottomColor:'#AAA'
    },
	bottomDiv: {
		width: width(100),
		height: 80,
		backgroundColor: '#FFF',
		alignItems: 'center',
	},
	bottomInner: {
		width: width(95),
		height: 65,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	inputField: {
		width: width(40),
		height: 65,
	},
	shadowImg: {
		width: width(100),
		height: 260,
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	boxShadow: {
		borderTopLeftRadius: 12,
		borderTopRightRadius: 12,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 12,
		elevation: 12,
		backgroundColor: 'transparent',
    },
    boxShadowNav: {
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#AAA',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 12,
        elevation: 24,
        backgroundColor: 'transparent',
    },
	boxShadowBtn: {
		borderRadius: 8,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor: 'transparent',
	},
	bidButton: {
		width: 150,
		height: 45,
		backgroundColor: 'transparent',
	},
	font: {
		fontSize: 16,
		color: '#999',
	},
	backIcon: {
		width: 20,
		height: 16,
	},
	time: {
		width: 100,
		height: 20,
		backgroundColor: '#b88581',
		borderBottomLeftRadius: 5,
		borderTopRightRadius: 5,
		justifyContent: 'center',
		alignItems: 'center',
	},
	small: {
		fontSize: 12,
		color: '#999',
		
    },
    big: {
        fontSize: 20,
        fontWeight:'bold',
        color:'black',
    }
});

const mapStateToProps = store => ({
	user: store.currentUser,
	products: store.allProduct,
	currentProductNav: store.currentProductNav,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
      Get_Ending_Product,
      Get_Mostview_Product,
      Get_Mostbid_Product,
      Submit_Bid
  }, dispatch);
}
//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);
