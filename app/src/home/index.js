//import liraries
import React from 'react';
import { StackNavigator } from 'react-navigation';

import Home from './list';
import Details from './detail';
import { transitionConfig } from '../global';
const HomeNav = StackNavigator(
	{
		Home: {
			screen: Home,
		},
		Detail: {
			screen: Details,
			navigationOptions: {
        tabBarVisible: false
      }
		},
	},
	{
		headerMode: 'none',
		initialRouteName: 'Home',
		navigationOptions: {
			gesturesEnabled: false,
		},
		transitionConfig,
	}
);

export default HomeNav;