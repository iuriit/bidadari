//import liraries
import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Platform,
	ScrollView,
	ImageBackground,
	Image,
	TouchableOpacity,
	TextInput,
	Keyboard,
} from 'react-native';
import * as ducks from '../../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
import { bindActionCreators } from 'redux';
import images from '../../const/images';

import Switch from 'react-native-switch-pro';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// create a component
class PaymentScreen extends Component {
	constructor() {
		super();

		this.state = {};
	}
    renderProduct() {
        const {item} = this.props.navigation.state.params;
        return (

            <View style={[styles.boxShadow, { flexDirection: 'row', marginBottom: 8 }]} >
                    <View style={styles.touchableDiv}>
                        <View style={styles.innerDiv}>
                            <View style={{ borderRightWidth: 0.5, borderRightColor: '#AAA', marginRight: 10 }}>
							{
								item.image !== null ?
									<Image source={{ uri: "http://13.250.103.147/" + item.image }} style={{ width: 100, height: 60 }} resizeMode="contain" />
								:null
							}
                            </View>
                            <View style={styles.detailsDiv}>
                                <Text style={{ fontSize: 14, color: 'black' }}>{item.name}</Text>
                                <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Text style={styles.small}>PURCHASE PRICE</Text>
                                    <Text style={{ color: '#c19592', fontSize: 12 }}>$ {item.cost}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
            </View>
        )
    }
	render() {
		return <View style={styles.container}>
		
				<ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
					<KeyboardAwareScrollView extraHeight={Platform.select({android : 160, ios:0}) } automaticallyAdjustContentInsets={false} enableOnAndroid={true} keyboardShouldPersistTaps='handled'>
						<View style = {styles.background}>
							<View style={styles.headerDiv}>
								<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: width(95), marginTop: Platform.select(
											{ ios: 16, android: 0 }
										) }}>
									<TouchableOpacity style={{ width: 40, alignItems:'center', justifyContent:'center', marginLeft: 5, marginTop: 16, }} onPress={() => {
										this.props.navigation.goBack();
										Keyboard.dismiss();
										}}>
										<Image source={images.backIcon} style={styles.backIcon} resizeMode="stretch" />
									</TouchableOpacity>
								</View>
							</View>
							<View style={styles.mainDiv}>
								<Text style={[styles.big, { fontSize: 20, marginBottom: 0 }]}>Payment for</Text>
								{this.renderProduct()}
								<Text style={[styles.big, { fontSize: 14 }]}>CARD DETAILS</Text>

								<View>
									<View style={{ flexDirection: 'row' }}>
										<View style={{ width: width(40), marginRight: width(5) }}>
											<Text style={{ fontSize: 11, color: '#AAA' }}>NAME</Text>
											<TextInput style={styles.input} placeholder="Name" value="Martin" underlineColorAndroid="transparent" autoCapitalize="none" />
										</View>
										<View style={{ width: width(45) }}>
											<Text style={{ fontSize: 11, color: '#AAA' }}>SURENAME</Text>
											<TextInput style={styles.input} placeholder="Surename" value="Montgomery" underlineColorAndroid="transparent" autoCapitalize="none" />
										</View>
									</View>
									<View style={{ marginTop: 20 }}>
										<Text style={{ fontSize: 11, color: '#AAA' }}>CARD NUMBER</Text>
										<View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: '#AAA' }}>
											<TextInput style={[styles.input, { width: width(85) - 20, borderBottomWidth: 0 }]} placeholder="Address" underlineColorAndroid="transparent" autoCapitalize="none" value="1234 5678 9009 8765" />
											<Image source={images.visa} style={{ width: 40, height: 20 }} resizeMode="contain" />
										</View>
									</View>
									<View style={{ flexDirection: 'row' }}>
										<View style={{ marginTop: 20, width: width(40), marginRight: width(5) }}>
											<Text style={{ fontSize: 11, color: '#AAA' }}>EXP DATE</Text>
											<TextInput style={styles.input} placeholder="Name" value="06/20" underlineColorAndroid="transparent" autoCapitalize="none" />
										</View>
										<View style={{ marginTop: 20, width: width(45) }}>
											<Text style={{ fontSize: 11, color: '#AAA' }}>CVV</Text>
											<View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderBottomColor: '#AAA', justifyContent: 'space-between' }}>
												<TextInput style={[styles.input, {borderBottomWidth:0, width:width(40) - 20}]} placeholder="Surename" value="476" underlineColorAndroid="transparent" autoCapitalize="none" />
												<Image source={images.symbol} style={{ width: 20, height: 20 }} resizeMode="contain" />
											</View>
										</View>
									</View>
								</View>
								<View style={{ width: width(90), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
									<Text>Save this card</Text>
									<Switch width ={47} height = {25} backgroundActive= "#e9c6c1" />
								</View>
							</View>
							
							<View style={{ flex: 1, width: width(90), justifyContent: 'center'}}>
								<TouchableOpacity style = {styles.boxShadow} 
									onPress={() => 
										{ 
										this.props.navigation.goBack();
										Keyboard.dismiss();
									}}>
									<ImageBackground source = {images.payNowBtn} style = {{alignItems:'center', justifyContent:'center', width:width(90), height:40}} resizeMode = "stretch" >
										<Text style = {{color:"white", fontSize:14, backgroundColor:'transparent', fontWeight:'bold',}}>Pay Now</Text>
									</ImageBackground>
								</TouchableOpacity>
							</View>
						</View>
					</KeyboardAwareScrollView>
				</ImageBackground>
			
			</View>;
	}
}

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	headerDiv: {
		height: height(10),
		width: width(100),
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	backIcon: {
		width: 20,
		height: 16,
	},
	background: {
		width: width(100),
		height: height(100),
		alignItems: 'center',
	},
	mainDiv: {
		width: width(90),
		height: Platform.select({ ios: height(83) - 60, android: height(80) - 70 }),
        justifyContent: 'space-between',
        // backgroundColor:'yellow',
		marginTop: 10,
	},
	boxShadow: {
		borderRadius: 8,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor: 'transparent',
	},
	boxShadowBtn: {
		borderRadius: 2,
		borderColor: '#ddd',
		borderBottomWidth: 0,
		shadowColor: '#AAA',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor: 'transparent',
	},
	big: {
		fontSize: 20,
		fontWeight: 'bold',
		color: 'black',
	},
	small: {
		fontSize: 10,
		color: '#999',
	},
	touchableDiv: {
		width: width(90),
		height: 90,
		backgroundColor: '#FFF',
		borderRadius: 8,
		justifyContent: 'center',
		// marginBottom:14
	},
	innerDiv: {
		flexDirection: 'row',
		width: width(90),
		height: 60,
		// backgroundColor:'yellow'
	},
	detailsDiv: {
		width: width(55),
		height: 60,
		justifyContent: 'center',
	},
	input: {
		height: 40,
		borderBottomWidth: 0.5,
		borderBottomColor: '#AAA',
	},
});

const mapStateToProps = store => ({

});

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{

			isAuthUpdated: ducks.isAuthUpdated,
		},
		dispatch
	);
};
//make this component available to the app
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PaymentScreen);
