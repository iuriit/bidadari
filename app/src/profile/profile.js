//import liraries
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    ScrollView,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
    YellowBox
} from 'react-native';
import * as ducks from '../../reducers/index';
import { connect } from 'react-redux';
import { width, height } from 'react-native-dimension';
import { bindActionCreators } from 'redux';
import images from '../../const/images';
import Swipeout from 'react-native-swipeout';
import {Get_Wonauctions} from '../../actions/product';
// create a component
class ProfileScreen extends Component {
    constructor() {
        YellowBox.ignoreWarnings(
    
            ['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'
        
        ]);
        super();
        
        this.state = {
            data: [],
        };

    }

    componentWillMount(){
        const {Get_Wonauctions, user} = this.props;
        
        Get_Wonauctions(user.token)
        .then({

        })
        .catch({

        })
        const array = [
            
        ];
        this.setState({ data: array});
    }
    componentWillReceiveProps(nextProps) {
        const { user, wonAuctions } = nextProps;
        const array = [ ];
        wonAuctions.map((item, index) =>{
            array.push({image: item.image, name:item.name, cost:item.winner_price, status:1});
        })
        this.setState({ data: array });
    }
    renderProduct(item, index){
        var swipeoutBtns = [
            {
                component: (
                    <View style = {{width:"100%", height:"100%", backgroundColor:'#b88581', alignItems:'center', justifyContent:'center'}}>
                        <Image source = {images.payIcon} style = {{width:18, height:18}} resizeMode="contain" />
                        <Text style = {{fontSize:10, color:'#fff'}}>Pay</Text>
                    </View>
                ),
                onPress: ()=> {
                    this.props.navigation.navigate('Payment', {item});
                }
            }
        ]
        return(
            
            <View style={[styles.boxShadow, { flexDirection: 'row', marginBottom: 14}]} key = {index}>
                <Swipeout
                    key = {index}
                    right={swipeoutBtns}
                    backgroundColor='transparent'
                    autoClose = {true}
                    buttonWidth = {55}
                    disabled = {item.status == 1 ? false : true}
                    
                >
                <TouchableOpacity style={styles.touchableDiv}>
                    <View style={item.status === 1?styles.waiting:styles.delivery}>
                        <Text style={[styles.small, { color: '#FFF' }]}>{item.status === 1 ? 'Waiting for Payment' : 'In Delivery'}</Text>
                    </View>
                    <View style={styles.innerDiv}>
                        <View style={{ borderRightWidth: 0.5, borderRightColor: '#AAA', marginRight: 10 }}>
                        {
                            item.image !== null ?
                            <Image source={{ uri: "http://13.250.103.147/" + item.image }} style={{ width: 100, height: 60 }} resizeMode="contain" />
                            :null
                        }
                        </View>
                        <View style={styles.detailsDiv}>
                            <Text style={{ fontSize: 14, color: 'black' }}>{ item.name }</Text>
                            <View style={{ marginTop: 5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={styles.small}>PURCHASE PRICE</Text>
                                <Text style={{ color: '#c19592', fontSize: 12 }}>$ {item.cost}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
               </Swipeout>
            </View>
        )
    }
    render() {
        const { user, wonAuctions } = this.props;
        const {data} = this.state;
        return (
            <View style={styles.container}>
                <ImageBackground source={images.background_login} style={styles.background} resizeMode="stretch">
                    <View style = {styles.profileInfo}>
                        {
                            user.image !== null ?
                            <Image source = {user.image !== "" ? {uri: "http://13.250.103.147/" + user.image} : images.photo2} style = {styles.photo} resizeMode = "stretch" />
                            :null
                        }
                        <View style = {{marginLeft:20,}}>
                            <Text style = {styles.big}>{user.name} {user.surname}</Text>
                            <Text style = {[styles.small, {marginTop:5, fontSize:12}]}>FatalStudioDesign</Text>
                        </View>
                    </View>
                    <View style={styles.statusDiv}>
                        <View style={{ width: width(28), height: 50, marginRight: width(2), justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={styles.small}>NUMBER OF TOKENS</Text>
                            <Text style={[styles.big, { color: '#b88581' }]}>{user.token_count}</Text>
                        </View>
                        <View style={{ width: width(28), height: 50, marginRight: width(2), justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={styles.small}>CURRENT BID</Text>
                            <Text style={[styles.big, { color: '#b88581' }]}>{user.bid_count}</Text>
                        </View>
                        <View style={{ width: width(28), height: 50, marginRight: width(2), justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={styles.small}>WON AUCTIONS</Text>
                            <Text style={[styles.big, { color: '#b88581' }]}>{wonAuctions === null ? "0" : wonAuctions.length}</Text>
                        </View>
                    </View>
                    <View style = {{width:width(90), backgroundColor:'transparent'}}>
                        <Text style = {[styles.big, {marginBottom:10, marginTop:10, fontSize:14, backgroundColor:'transparent'}]}>WON AUCTIONS</Text>
                    </View>
                    <ScrollView >
                    {
                        data.map((item, id)=>{
                            return this.renderProduct(item, id);
                        })
                    }
                    </ScrollView>
                </ImageBackground>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    profileInfo: {
        width:width(90),
        height:80,
        // backgroundColor:'yellow',
        marginTop: 50,
        flexDirection:'row',
        alignItems:'center',
    },
    photo:{
        width:80,
        height:80,
        borderRadius:10,
    },
    background: {
        width: width(100),
        height: height(100),
        alignItems: 'center',
    },
    statusDiv: {
        flexDirection: 'row',
        width: width(90),
        height: 60,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#AAA',
        marginTop:20,
    },
    big: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
    },
    small: {
        fontSize: 10,
        color: '#999',

    },
    touchableDiv: {
        width:width(90),
        height:90,
        backgroundColor:'#FFF',
        borderRadius: 8,
        // marginBottom:14
    },
    waiting:{
        width:120, 
        height:18,
        backgroundColor:'grey',
        borderBottomLeftRadius:5,
        borderTopRightRadius:5,
        alignSelf:'flex-end',
        justifyContent:'center',
        alignItems:'center',
    },
    delivery: {
        width:80, 
        height:18,
        backgroundColor:'#b88581',
        borderBottomLeftRadius:5,
        borderTopRightRadius:5,
        alignSelf:'flex-end',
        justifyContent:'center',
        alignItems:'center',
    },
    boxShadow: {
        borderRadius: 8,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#AAA',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 2,
        backgroundColor: 'transparent',
    },
    innerDiv: {
        flexDirection:'row',
        width:width(90),
        height:60,
        // backgroundColor:'yellow'
    },
    detailsDiv: {
        width:width(55),
        height:60,
        justifyContent:'center'
    }

    
    
});

const mapStateToProps = store => ({
    wonAuctions: store.currentAuctions,
    user: store.currentUser,
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        Get_Wonauctions
    }, dispatch);
}
//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileScreen);
