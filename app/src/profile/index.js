//import liraries
import React from 'react';
import { StackNavigator } from 'react-navigation';

import ProfileScreen from './profile';
import PaymentScreen from './payment';
import { transitionConfig } from '../global';
const ProfileNav = StackNavigator(
	{
		ProfileHome: {
			screen: ProfileScreen,
		},
		Payment: {
            screen: PaymentScreen,
		    navigationOptions: {
		        tabBarVisible: false
		    }
		},
	},
	{
		headerMode: 'none',
        // initialRouteName: 'ProfileHome',
		navigationOptions: {
			gesturesEnabled: false,
		},
		transitionConfig,
	}
);

export default ProfileNav;