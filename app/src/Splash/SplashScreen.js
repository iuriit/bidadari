//import liraries
import React, { Component } from 'react';
import { ImageBackground, Image, StyleSheet } from 'react-native';

import images from "../../const/images";

// create a component
class SplashScreen extends Component {
  render() {
    return (
      <ImageBackground
        source={images.background1}
        style={styles.background}
        resizeMode='cover'
      >
        
      </ImageBackground>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: '100%',
    height: 100
  }
});

//make this component available to the app
export default SplashScreen;
