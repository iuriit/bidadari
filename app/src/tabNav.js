//import liraries
import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';

import * as ducks from '../reducers/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import images from "../const/images";
import { width, height } from 'react-native-dimension';

// create a component
class BottomNav extends Component {
  constructor(props){
      super(props);
      const { navIndex } = this.props;
      this.state = {
          flag: navIndex,
          
      }
  }
  render() {
    const {updateNavId, navIndex} = this.props;
    return (
        <View style = {styles.container}>
            <ImageBackground source = {images.conbinedShape} style = {styles.background} resizeMode = "stretch">
                <View style = {{width:width(90), height:50, marginTop:24, flexDirection:'row', justifyContent:'space-between'}}>
                    <View style = {{width:100, height:50, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                        <TouchableOpacity style = {{alignItems:'center'}} onPress = {() => {
                            this.setState({ flag : 1});
                            updateNavId(1);
                            this.props.navigation.navigate('HOME');
                        }}>
                            <Image source = {this.state.flag === 1 ? images.home_active : images.home_inactive} style = {styles.icon} resizeMode = "contain" />
                            <Text style = {this.state.flag === 1 ? styles.active_font : styles.inactive_font}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center' }} onPress = {() => {
                            updateNavId(2);
                            this.setState({ flag : 2});
                        }}>
                            <Image source = {this.state.flag === 2 ? images.wishlist_active : images.wishlist_inactive} style = {styles.icon} resizeMode = "contain" />
                            <Text style = {this.state.flag === 2 ? styles.active_font : styles.inactive_font}>Wishlist</Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {{width:100, height:50, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}}>
                        <TouchableOpacity style = {{alignItems:'center'}} onPress = {() => {
                            updateNavId(3);
                            this.setState({ flag : 3});
                            this.props.navigation.navigate('PROFILE');
                        }}>
                            <Image source = { this.state.flag === 3 ? images.profile_active : images.profile_inactive} style = {styles.icon} resizeMode = "contain" />
                            <Text style = {this.state.flag === 3 ? styles.active_font: styles.inactive_font}>Profile</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center' }} onPress = {() => {
                            updateNavId(4);
                            this.setState({ flag : 4});
                        }}>
                            <Image source = {this.state.flag === 4 ? images.setting_active : images.setting_inactive} style = {styles.icon} resizeMode = "contain" />
                            <Text style = {this.state.flag === 4 ? styles.active_font : styles.inactive_font}>Settings</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
            <TouchableOpacity style = {styles.chat}>
                <Image source = {images.chat} style = {styles.chat} resizeMode = "contain"></Image>
            </TouchableOpacity>
        </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
	container: {
		height: 100,
		bottom: 0,
		left: 0,
		right: 0,
        position: 'absolute',
        alignItems:'center',
    },
    chat: {
        width:70,
        height:70,
        position: 'absolute',
    },
	background: {
        marginTop:8,
		width: width(100),
		height: 100,
        // backgroundColor: 'yellow',
        alignItems: 'center',
    },
    icon: {
        width:30,
        height:30,
    },
    active_font:{
        color:'#e9c6c0',
        fontSize:10,
        fontWeight:'bold',
        textAlign:'center'
    },
    inactive_font:{
        color: '#9d9b9b',
        fontSize: 10,
        fontWeight:'bold',
        textAlign:'center'
    }
});

//make this component available to the app
const mapStateToProps = store => ({
    navIndex: store.currentNavId
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        updateNavId: ducks.updateNavId,

    }, dispatch);
};

//make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BottomNav);
